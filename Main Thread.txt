^This ^post ^will ^be ^constantly ^updated, [^currently ^at ^v20180830a ](https://gitlab.com/Aconitin/IOTA-AllInOne-Thread/)^. ^(If you miss something, let me know!)

___
___
___
# IOTA in a Nutshell

&nbsp;

IOTA is a *free* and *scaleable* transaction settlement and data transfer layer for the Internet of Things (IoT). It is based on a new distributed ledger technology, the Tangle, which overcomes the inefficiencies of current Blockchain designs and introduces a new way of reaching consensus in a decentralized peer-to-peer system.

&nbsp;

___
___
# Official Resources

&nbsp;

**[IOTA Website](https://www.iota.org/), including:**

* IOTA [Whitepaper](https://www.iota.org/research/academic-papers) by Dr. Serguei Popov as well as more IOTA-related research.
* IOTA [Blog](https://blog.iota.org) - Official news outlet.
* IOTA [Ecosystem Fund](https://ecosystem.iota.org/) & [Transparency Tracker](https://transparency.iota.org/) - Circa 21 Ti [assigned to support community projects](https://blog.iota.org/iota-ecosystem-fund-2-million-f6ade6a4d8ba)!
* IOTA [Data Marketplace](https://data.iota.org/) - Experimental project on the IOTA testnet.
* IOTA [Docs](https://docs.iota.org/introduction) - Documentation & Tutorials.
* IOTA [FAQ](https://www.iota.org/get-started/faqs) - Please read this, as well as [this](https://www.reddit.com/r/Iota/comments/7irl3l/information_and_faq/) excellent FAQ. You can also find an introductory FAQ [in this comment](https://www.reddit.com/r/Iota/comments/8kfrxk/iota_allinone_thread/e0en01n/).
* [Qubic](https://qubic.iota.org) - Oracle Machines, Smart Contracts, Outsourced Computations and more!
	
&nbsp;

**Media**

* IOTA [Discord](https://discordapp.com/channels/397872799483428865) - Main communication platform.
* IOTA [Twitter](https://twitter.com/iotatoken) @iotatoken - Use hashtag [\#iota](https://twitter.com/search?q=%23iota) or [\#iotastrong](https://twitter.com/search?q=%23iotastrong). There is also a dedicated [engineering Twitter](https://twitter.com/iota_dev?lang=de).
* IOTA [Stackexchange](https://iota.stackexchange.com/) - For the really tough questions!
* IOTA [Github](https://github.com/iotaledger) - Contains [IOTA Reference Implementation (IRI)](https://github.com/iotaledger/iri) in Java, libraries in [JavaScript](https://github.com/iotaledger/iota.lib.js), [Python](https://github.com/iotaledger/iota.lib.py), [Go](https://github.com/iotaledger/giota), [C\#](https://github.com/iotaledger/iota.lib.csharp) and more!

&nbsp;

**Wallets**

* [Trinity Wallet](https://trinity.iota.org) for iOS, Android, Windows, Mac and Linux. Currently in [open Beta](https://www.reddit.com/r/Iota/comments/8n15mo/trinity_mobile_beta_release/), yet fully functional (see [Development Roadmap](https://trinity.iota.org/roadmap/)). Desktop version [was released into Beta on August 25](https://blog.iota.org/trinity-desktop-beta-release-e6bb81ab88a0).
* [Stateless Wallet](https://github.com/iotaledger/wallet/releases) for Windows, Mac OS X & Linux. Semi-deprecated, yet secure. If you can, please use Trinity instead. Please also note that as of May 31, 2018, the old [Android Wallet](https://github.com/iotaledger/android-wallet-app#iota-android-app---deprecated) is deprecated and should no longer be used!

&nbsp;

___
___
# Current Events, News & Noteworthy Stuff

&nbsp;

* *Ongoing* - The Tangle is currently experiencing network reliability issues. The IF is working on a solution, please be patient. Check [here](https://github.com/iotaledger/iri/issues/895) for updates!

&nbsp;

* *August 30, 2018* - Welcome [Marwen Trabelsi](https://blog.iota.org/welcome-marwen-trabelsi-to-the-iota-foundation-5bf7d6840824) to the IOTA Foundation!
* *August 29, 2018* - Welcome [Brord van Wierst](https://blog.iota.org/welcome-brord-van-wierst-to-the-iota-foundation-801cc4314db1) to the IOTA Foundation!
* *August 28, 2018* - Welcome [Janine Hrtel](https://blog.iota.org/welcome-janine-h%C3%A4rtel-to-the-iota-foundation-219278179239) to the IOTA Foundation!
* *August 28, 2018* - Welcome [Hans Moog](https://blog.iota.org/welcome-hans-moog-to-the-iota-foundation-ff8c6d64158b) to the IOTA Foundation!
* *August 26, 2018* - [German] Weekly Sunday Banter released [on Youtube](https://www.youtube.com/watch?v=NLk-Xme8MXQ). The English Sunday Banter was cancelled permanently.
* *August 25, 2018* - The [Trinity Desktop Wallet was released](https://blog.iota.org/trinity-desktop-beta-release-e6bb81ab88a0) in Beta! You can download it from [the official Trinity site](https://trinity.iota.org/). Discussion on Reddit can be found [here](https://www.reddit.com/r/Iota/comments/9a5ivr/trinity_desktop_beta_has_been_released/).
* *August 18, 2018* - Welcome [Casper Eicke Frederiksen](https://blog.iota.org/welcome-casper-eicke-frederiksen-to-the-iota-foundation-c7f51dc59dc) to the IOTA Foundation!
* *August 16, 2018* - Welcome [Rihards Gravis](https://blog.iota.org/welcome-rihards-gravis-to-the-iota-foundation-566ac1ab3f70) to the IOTA Foundation
* *August 15, 2018* - [The First Cohort of Ecosystem Development Fund Grantees](https://blog.iota.org/the-first-cohort-of-ecosystem-development-fund-grantees-e9da89ecfb56) has been announced! Discussion can be found [here](https://www.reddit.com/r/Iota/comments/97kipt/the_first_cohort_of_ecosystem_development_fund/).
* *August 11-14, 2018* - [This Pastebin](https://pastebin.com/index/482HVvPu) was leaked on Discord, sparked a lot of drama, [an official statement](https://files.iota.org/comms/IOTA+Foundation+Update+from+the+Board+of+Directors+and+Founders.pdf), the signing of an [official memorandum of understanding](https://www.reddit.com/r/Iota/comments/9745h0/memorandum_of_understanding_has_been_signed_by/), a [smorgasbord of negative press](https://www.google.com/search?q=iota+chat+leak&ie=utf-8&oe=utf-8&client=firefox-b-ab) and finally, the official inauguration of both [Serguei Popov](https://blog.iota.org/welcome-serguei-popov-to-the-iota-foundation-board-of-directors-afcdcf20a032) as well as [Sergey "Come-from-Beyond" Ivancheglo](https://blog.iota.org/welcome-sergey-ivancheglo-to-the-iota-foundation-board-of-directors-2167ca99e495) into the IOTA Foundation Board of Directors (Reddit threads [here](https://www.reddit.com/r/Iota/comments/97c0gm/welcome_serguei_popov_to_the_iota_foundation/) and [here](https://www.reddit.com/r/Iota/comments/97bz0o/welcome_sergey_ivancheglo_to_the_iota_foundation/), respectively). Please wholeheartedly welcome both of them!
* *August 04, 2018* - Welcome [Aaron Alsop](https://blog.iota.org/welcome-aaron-alsop-to-the-iota-foundation-6cc88fd81d41) to the IOTA Foundation!
* *August 03, 2018* - [Qubic Status Update](https://blog.iota.org/qubic-status-update-august-3rd-2018-73468079d414)!
* *August 03, 2018* - [IOTA Hub](https://blog.iota.org/introducing-iota-hub-5349bb8a29cd) released. Reddit thread is [here](https://www.reddit.com/r/Iota/comments/9481bx/introducing_iota_hub/), GitHub link [here](https://github.com/iotaledger/rpchub)!
* *August 01, 2018* - [Welcome Dave De Fijter to the IOTA Foundation](https://blog.iota.org/welcome-dave-de-fijter-to-the-iota-foundation-d52d0ef94204)!
* *July 31, 2018* - [The Origins of JINN and IOTA](https://helloiota.com/origins-of-jinn-and-iota/)
* *July 30, 2018* - [Interview with Robert Lie](https://iotahispano.com/2018/07/30/interview-with-robert-lie-mobilefish/) on IOTA HISPANO!
* *July 25, 2018* - Audi Think Tank & IOTA [explore Tangle-based mobility use cases](https://blog.iota.org/audi-think-tank-iota-foundation-explore-tangle-based-mobility-use-cases-29d43e1cf51)!
* *July 24, 2018* - Welcome [Anes Hodic](https://blog.iota.org/welcome-anes-hod%C5%BEi%C4%87-to-the-iota-foundation-2864f95b91b4) to the IOTA Foundation!
* *July 19, 2018* - IOTA [wins the blockchain / AI category](https://twitter.com/AlexanderRenz/status/1019970609508503552) at StartupAutobahn PlugAndPlay in Stuttgart.
* *July 17, 2018* - [Qubic AMA](https://www.reddit.com/r/Iota/comments/8zcip4/qubic_team_ama_july_17th/) with the Qubic team. First annoucement can be found [here](https://www.reddit.com/r/Iota/comments/8pwrlp/qubic_ama_soon/), summary is [here](https://www.reddit.com/r/Iota/comments/8zoudh/highlights_of_iota_qubic_ama/).
* *July 17, 2018* - This post reached 111.000 views and 500 likes. Thank you!
* *July 16, 2018* - [Interview with Daniel Trauth](http://www.iotahispano.com/2018/07/16/interview-with-daniel-trauth-head-of-the-machine-learning-in-production-engineering-working-group-at-the-wzl/), Chief Engineer and Head of the Machine Learning in Production Engineering working group at the WZL, by [IOTA Hispano](http://www.iotahispano.com)!
* *July 12, 2018* - 5PM CET: [Chat with the board of the IOTA Foundation](https://www.reddit.com/r/Iota/comments/8wj9wl/you_are_invited_to_the_2nd_chat_with_the_board_of/). You can find the video [here](https://www.youtube.com/watch?v=p7cdV_hsHAc&feature=youtu.be)!
* *July 09, 2018* - There will be a snapshot on Monday, July 9th, 08:00 UTC until 15:00 UTC, see [here](https://twitter.com/i/web/status/1014595645640978432)!
* *July 06-13, 2018* - [London Fintech Week 2018](https://www.fintechweek.com/)!
* *July 07, 2018* - [Blog Post] Green light [from the EU Commission for IOTA and the European smart city consortium +CityxChange](https://blog.iota.org/green-light-from-the-eu-commission-for-iota-and-the-european-smart-city-consortium-cityxchange-f7928aef33ac)
* *July 07, 2018* - [Blog Post] [Trinity Wallet Summer Update](https://blog.iota.org/trinity-wallet-summer-update-6e9514293268)
* *July 07, 2018* - publicIOTA [Interview with Gabriela Jara](http://publiciota.com/iota-hispano), IEN member from Argentina and IOTA Hispano writer.
* *July 05, 2018* - Livestream of the [IOTA Luxembourg Meetup](https://www.youtube.com/watch?v=SiwyADnSFG4), see event page [here](https://ecosystem.iota.org/events/luxembourg-iota-meetup)!
* *July 04, 2018* - \[German\] [IOTA Talk](https://www.youtube.com/watch?v=wELKlmT2Mv0) at Webmontag Frankfurt #92 by Marcel Engelmann!
* *July 03, 2018* - First monthly [Qubic Update](https://blog.iota.org/qubic-status-update-july-3rd-2018-e176ee132b79) written by Eric Hop!
* *July 02, 2018* - [Welcome Dr. Klaus Schaaf](https://blog.iota.org/welcome-klaus-schaaf-to-the-iota-foundation-5e823b8b91e1) to the IOTA Foundation!
* *July 02, 2018* - [Interview with Dr. Julie Maupin](https://helloiota.com/interview-with-julie-maupin-iota-foundation/)!
* *July 02, 2018* - [Interview with Nuriel Shem Tov](http://www.iotahispano.com/2018/07/02/interview-with-nuriel-shem-tov/)!
* *July 01, 2018* - This Thread reached 90000 views (15000 per week). The "partnerships & affiliations"-section has been outsourced to the excellent [IOTA Archive](http://iotaarchive.com/). New sections are WIP.
* *June 29, 2018* - Trinity Wallet [Update Changelog](https://www.reddit.com/r/Iota/comments/8usddp/trinity_wallet_update_29th_june/) for this week released.
* *June 29, 2018* - Welcome [Mark Schmidt](https://blog.iota.org/welcome-mark-schmidt-to-the-iota-foundation-5e538595c942) to the IOTA Foundation!
* *June 28, 2018* - [Networking Mixer](https://www.meetup.com/Oslo-IOTA-Meetup/events/251248228/) in Oslo!
* *June 28, 2018* - [What's up with the Tangle](https://medium.com/@lewisfreiberg/whats-up-with-the-tangle-d825c692e7a8) by [Lewis Freiberg](https://medium.com/@lewisfreiberg)!
* *June 27, 2018* - [Announcing the Development of Qubic Lite](https://medium.com/@micro_hash/announcing-the-development-of-qubic-lite-a-community-implementation-of-the-qubic-protocol-d526093459d8), a Community Implementation of the Qubic Protocol. The website is live and can be found [here](http://qubiclite.org/).
* *June 22, 2018* - [IOTA and SinoPac team up to collaborate on digital innovations in the financial sector](https://blog.iota.org/iota-and-sinopac-team-up-to-collaborate-on-digital-innovations-in-the-financial-sector-53472176554d)!
* *June 21, 2018* - [Tip Selection Algorithm Update](https://blog.iota.org/new-tip-selection-algorithm-in-iri-1-5-0-61294c1df6f1) on the IOTA Blog.
* *June 19, 2018* - [Interview](http://www.iotahispano.com/2018/06/19/exclusive-interview-with-terry-shane/) with Terry Shane, President and CEO of [Refined Data Solutions](https://www.refineddata.com/) and Founder of the [bIOTAsphere](http://www.biotasphere.com/)!
* *June 19, 2018* - This thread reached 70.000 views!
* *June 19-20, 2018* - [German 2b AHEAD ThinkTank Congress](https://www.zukunft.business/deutschlands-innovativster-zukunftskongress/bewerbung/) - [Speech by Dominik Schiener](https://twitter.com/DomSchiener/status/1003699649834831872?s=19)!
* *June 18, 2018* - [Holger Kther](https://blog.iota.org/welcome-holger-k%C3%B6ther-to-the-iota-foundation-72c2eba8c41a) joins IOTA Foundation as Head of Partnership Relations!
* *June 17, 2018* - [Interview with Steffen "Limo" Vogt](http://www.iotahispano.com/2018/06/17/exclusive-interview-with-steffen-vogt-limo-tangleblog/)!
* *June 16, 2018* - Current IOTA [R&D Projects](https://blog.iota.org/whats-next-current-iota-r-d-projects-eeb8cc03adb5) blog post!
* *June 15-26, 2018* - [World Blockchain Summit](https://frankfurt.worldblockchainsummit.com/) with speaker Julie Maupin!
* *June 15, 2018* - German [Interview with Tobias Zeitler](https://www.youtube.com/watch?v=a2-yHJxScw4).
* *June 14, 2018* - Welcome [Fahad Sheikh](https://blog.iota.org/welcome-fahad-sheikh-to-the-iota-foundation-498ba2c01ae5) to the IOTA Foundation!
* *June 14, 2018* - IOTA on Ledger Nano S: [Alpha released on testnet](https://www.reddit.com/r/Iota/comments/8qwbe3/iota_on_ledger_nano_s_alpha_released_on_testnet/)!
* *June 14, 2018* - [IOTA trading now available on Bitpanda](https://twitter.com/i/web/status/1007171519137681409)!
* *June 12, 2018* - [CEBIT 2018: Johann Jungwirth - Opening Keynote: Inspiration and Vision](https://www.cebit.de/speaker/johann-jungwirth/9391). Additionally, [Dominik](https://www.reddit.com/r/Iota/comments/8pjfby/domink_as_speaker_along_with_volkswagen/) will speak and [Volkswagen](https://twitter.com/JohannJungwirth/status/1005268618890956800?s=20) will present a proof-of-concept. Panel discussion [here](https://www.youtube.com/watch?v=8gusb1HUXIk).
* *June 11, 2018* - [Interview with Eric Hop](http://www.iotahispano.com/2018/06/11/interview-with-eric-hop-qubics-project-owner/), related: [WTF is Qubic?](https://www.youtube.com/watch?v=T6c-4bfR_Xo)
* *June 09, 2018* - Medium: [Economic Clustering and IOTA](https://medium.com/@comefrombeyond/economic-clustering-and-iota-d3a77388900) by CfB. Discussion [here](https://www.reddit.com/r/Iota/comments/8puqni/comefrombeyond_economic_clustering_and_iota/)!
* ~~*June 09, 2018* - [TEDx Talk](https://i.redditmedia.com/DkJDkybcj0BwsWSgCbIGHzLpTJfsBeA_7nyylw4v-5Y.jpg?s=dfa3789e742bc61160f7758518dac28c) by [Dominik Schiener](https://twitter.com/DomSchiener). You can find the discussion about this in [this](https://www.reddit.com/r/Iota/comments/8nyi25/tedx_talk_by_dominik_schiener_co_founder_iota/) reddit thread.~~ Cancelled, see [here](https://twitter.com/i/web/status/1003214117954613248).
* *June 07, 2018* - [German Interview with David](https://www.wired.de/collection/tech/iota-gruender-david-soensteboe-qubic-wird-sich-gegenueber-ethereum-durchsetzen)
* *June 07, 2018* - /r/iota reached 111.111 subscribers!
* *June 07, 2018* - [Excellent Summary of IOTA project risks](https://www.reddit.com/r/Iota/comments/8ow4br/iota_what_are_the_project_risks/) on reddit.
* *June 07, 2018* - [Edward Greve](https://www.youtube.com/watch?v=8L8UzRQfEug) talks about IF vision.
* *June 07, 2018* - [Keynote Speech at InnoVEX](https://twitter.com/InnoVEX3/status/999611568122834945) by ~~[Dominik Schiener](https://twitter.com/DomSchiener)~~ Replacement: [Edward Greve](https://twitter.com/InnoVEX3/status/1002142230339977216).
* *June 06, 2018* - [Interview with Darren "Green Protocol" Olson](http://www.iotahispano.com/2018/06/06/exclusive-interview-with-darren-olson-green-protocol/).
* *June 06, 2018* - [IOTA Launches Open Community Project IOTA Lab, Managed By AKITA](https://www.cointrust.com/news/iota-launches-open-community-project-iota-lab-managed-by-akita)
* *June 05, 2018* - [Money20/20 Europe](https://money2020europe2018.sched.com/event/Dnh7/dlt-reinventing-the-economy?iframe=no&w=100%&sidebar=yes&bg=no)
* *June 03, 2018* - [Qubic](https://qubic.iota.org/) updates & information published today. Explanatory articles [here](https://medium.com/coinmonks/qubic-quorum-based-computations-powered-by-iota-3770fbd62341), [here](https://www.reddit.com/r/Iota/comments/8obbuj/great_eli5_q_summary_from_twitter_franclukas1/), [here](https://www.tangleblog.com/2018/06/04/five-bullet-points-of-iota-qubic-and-its-insane-implications-and-consequences-interpreted/) and [here](https://medium.com/coinmonks/what-is-q-from-a-laymen-given-barney-style-6387b18267d2)!
* *June 03, 2018* - This thread reached 404 upvotes. Thank you for appreciating my work :)
* *June 03, 2018* - [German Interview](https://www.deutschlandfunknova.de/beitrag/iota-gruender-dominik-schiener-wir-machen-die-maschinen-wirklich-unabhaengig) with Dominik Schiener.
* *June 02, 2018* - [IOTA Foundation Supervisory Board AMA](https://www.youtube.com/watch?v=ArF1AVG0idU) livestream/video on Youtube. This was announced [here](https://twitter.com/iotatokennews/status/996099841389457411/photo/1?tfw_creator=CarolineWHarris&ref_src=twsrc%5Etfw&ref_url=https%3A%2F%2Fcryptocurrencynews.com%2Fdaily-news%2Fiota-news%2Fiota-news-miota-rises%2F) and [here](https://www.reddit.com/r/Iota/comments/8ns808/you_are_invited_a_chat_with_the_board_of_the_iota/), and you can find a nice text summary in [this](https://www.reddit.com/r/Iota/comments/8o2s17/text_summary_from_youtube_ama/) thread, as well as [this](https://www.reddit.com/r/Iota/comments/8o1usl/summary_of_best_thing_you_learned_or_heard_during/) one!
* *June 01, 2018* - [Trinity Wallet Tutorial](https://www.youtube.com/watch?v=Tp98yWNFV1g) by Mobilefish released.

^(Archive of noteworthy stuff at the bottom of this thread.)

&nbsp;

___
___
# \(Community\) Resources, Projects & Services on IOTA / the Tangle

&nbsp;

Since there are an incredibly vast number of projects, only the largest ones are listed here. Please have a look at the following sites to get a more complete overview:

* The [IOTALINK directory](https://iotalink.directory/), created and curated by [plotpanda](https://twitter.com/plotpandacom),
* [IOTAShops](http://www.iotashops.com/) and [Pay with IOTA](https://pay-with-iota.org/), two websites listing stores that accept IOTA as payment method,
* ...and of course the IOTA [Ecosystem Discover](https://ecosystem.iota.org/discover/projects) page!

&nbsp;

`Please note that these are unofficial, mostly non-audited, Third-Party projects.`

**Node Software, Hardware & Tools**

* The [DevIOTA](https://ecosystem.iota.org/projects/carriota-ecosystem) ecosystem by [SemkoDev](https://semkodev.com/), consisting of:

    [Field](https://gitlab.com/semkodev/field.cli), a node graph explorer, load balancer and incentiviser.

    [Romeo](https://github.com/SemkoDev/romeo.html), a lightweight ledger built on top of the IOTA Tangle.

    [Bolero](https://github.com/SemkoDev/bolero.lib), a set of tools that enable easy download/setup of IOTA IRI & Nelson P2P.

    [Nelson](https://gitlab.com/semkodev/nelson.cli), enabling automatic peer discovery for IOTA nodes.

    [Hercules](https://gitlab.com/semkodev/hercules) - IRI on steroids (Updates for [Apr 30](https://medium.com/deviota/carriota-hercules-ready-steady-6b858a708746), [May 23](https://medium.com/deviota/carriota-hercules-flexing-the-muscles-5869bf12332b))!

* Various Tangle explorers and visualizers, namely:

    The [Tangle explorer](https://thetangle.org/) with [live transactions](https://thetangle.org/live) and [statistics](https://thetangle.org/statistics),
	
    The [IOTA Tangle explorer](https://iotasear.ch/) by [iotasearch](https://twitter.com/iotasearch),
	
	A [Tangle explorer](https://iotatrail.com/) by [eukaryote](https://twitter.com/eukaryote314),
	
    A [Tangle visualizer](https://visualizer.iota-tangle.io/) by [iota-tangle.io](https://iota-tangle.io/),
	
    A [IOTA Tangle visualizer](http://tangle.glumb.de/) by [Maximilian Beck](https://twitter.com/BeckMaximilian),
	
    And the [Tangle Monitor](https://tanglemonitor.com/).
	
* The [iota.fm](http://iota.fm/) nodes by [grandcentrix](https://www.grandcentrix.net/), including:

    [balance.iota.fm](https://balance.iota.fm/), which monitors your address and notifies you on changes, and
	
    [pro.iota.fm](http://pro.iota.fm/), which promotes and reattaches your transactions!

* Other transaction helpers:

    [IOTAReatta.ch](http://www.iotareatta.ch/) promotes and reattaches your transactions!

    [Tangle Tools](https://tangletools.org/), which also promotes and reattaches your transactions!
	
* ... and last, but not least:
    
    [Nelium IOTA Wallet](https://www.nelium.io/) - A third-party IOTA wallet (watch their introductory video [here](https://www.youtube.com/watch?v=h7CyUlYkf2Y)).
	
    IOTA plug-and-play [full nodes](https://rock64iota.com/?product=rock64-120gb-ssd-full-iota-node) for sale!
	
    [IOTA Spam Fund](http://iotaspam.com/) - Support the tangle by spamming transactions and earn some iotas!

**Community & News**

* [IOTA Evangelist Network (IEN)](https://ien.io/) aiming to establish IOTA as a technical standard within the global market.
* [The Tangler](http://www.tangleblog.com/) - News and Information about IOTA, by [Limo](https://twitter.com/tangleblog). Includes a [weekly recap video](https://www.youtube.com/user/Tentorkel) and the [IOTA chronicle](https://www.iota-chronicle.com/) news website.
* [IOTAsupport](https://www.reddit.com/r/IOTASupport/) - Subreddit for all IOTA related problems.
* [IOTAmarkets](https://www.reddit.com/r/IOTAmarkets/) - Subreddit for price speculation.
* [HelloIota](https://forum.helloiota.com/) - Forum as well as [website](https://helloiota.com/) for all things IOTA
* [public IOTA](http://publiciota.com/), an independent group that aims to move IOTA forward.

**Education**

* [An illustrated introduction](https://blog.iota.org/the-tangle-an-illustrated-introduction-4d5eae6fe8d4) on the IOTA blog
* [Transactions, Confirmation and Consensus](https://github.com/noneymous/iota-consensus-presentation) presentation
* [Everything Tangle](https://www.youtube.com/channel/UCQaOR_QLI2tGceGAp3ZWfQw) - Youtube channel with nicely animated educational videos such as [this](https://www.youtube.com/watch?v=ivWqqfzunhI) and [this](https://www.youtube.com/watch?v=y7JPQng-Vjc)
* [Robert \"Mobilefish\" Lie](https://www.youtube.com/watch?v=MsaPA3U4ung&list=PLmL13yqb6OxdIf6CQMHf7hUcDZBbxHyza)'s technical tutorial series
* [IOTA Simply Explained](https://www.youtube.com/watch?v=CZxH1V_zoug)
* [Genesis](https://medium.com/@evanbblackwell/tangley-boi-tryout-a63ee1f976af) - Introduction to the Tangle
* [Tanglescout::Reader()](http://reader.tanglescout.net/) - List of IOTA/Tangle-related academic research!
* [IOTA101](https://hribek25.github.io/IOTA101/) Developer Essentials

**Exchanges**

`Please inform yourself before you invest in any cryptocurrency! Private key / seed security is of utmost importance! Do no use an online seed generator! Do not share your seed with anybody! Do not input your seed into any websites that you dont trust! You are your own bank, you are responsible for your tokens, and nobody can refund you if you loose your money.`

Like other cryptocurrency tokens, the [IOTA token](https://coinmarketcap.com/currencies/iota/) can be [bought and traded](https://medium.com/@fuo213/how-to-buy-iota-the-complete-guide-for-crypto-dummies-e63560caf921) on exchanges such as [Bitfinex](https://www.bitfinex.com/), [OKEx](https://www.okex.com/), [Coinone](https://coinone.co.kr/), [Binance](https://www.binance.com/), [Huobi](https://www.huobi.pro/de-de/), [Ovis](https://www.ovis.com.tr/), [Gate.io](https://gate.io/), [Exrates](https://exrates.me/dashboard), [HitBTC](https://hitbtc.com/), [CoinFalcon](https://coinfalcon.com/), [Cobinhood](https://cobinhood.com/home), [BitPanda](https://www.bitpanda.com/de) & [Omoku](https://de.omoku.io/).

The denomination on exchanges is [MIOTA](https://www.reddit.com/r/Iota/comments/4xkja2/iota_system_of_units/), which is one million IOTA tokens.

&nbsp;

___
___
# The Team

There are currently **65** people working at the IOTA Foundation. Out of these, **4** are founders, **46** are "regular employees" and **15** are in an advisory role.

&nbsp;

* [David Snsteb](https://medium.com/@DavidSonstebo), Co-Chairman of the Board of Directors & Founder ([Twitter](https://twitter.com/davidsonstebo/), [Medium](https://medium.com/@DavidSonstebo))
* [Dominik Schiener](https://medium.com/@domschiener), Co-Chairman of the Board of Directors & Founder ([Twitter](https://twitter.com/domschiener), [Medium](https://medium.com/@DomSchiener))
* [Sergey "Come\-from\-Beyond" Ivancheglo](https://twitter.com/c___f___b), Honorary Member & Founder ([Twitter](https://twitter.com/c___f___b), [Medium](https://medium.com/@comefrombeyond), [GitHub Gist](https://gist.github.com/Come-from-Beyond))
* [Serguei Popov](https://blog.iota.org/@serguei.popov), Research Mathematician & Founder

&nbsp;

* [Aaron Alsop](https://blog.iota.org/welcome-aaron-alsop-to-the-iota-foundation-6cc88fd81d41) - Product Owner
* [Alexey Sobolev](https://blog.iota.org/welcome-alexey-sobolev-to-the-iota-foundation-5fbec98b696e), Frontend Developer
* [Alisa Maas](https://blog.iota.org/welcome-alisa-maas-to-the-iota-foundation-9205118a3a23), Mobility & Automotive Stream Lead
* [Alon Elmaliah](https://blog.iota.org/meet-core-developer-alon-elmaliah-4cafa176464f), Staff Research Engineer
* [Alon Gal](https://blog.iota.org/welcome-alon-gal-to-the-iota-foundation-bca0ded3e433), Senior Research Engineer
* [Andrea Villa](https://blog.iota.org/welcome-andrea-villa-to-the-iota-foundation-62ad5f2ecaf1), Senior Research Engineer, Security Specialist
* [Andreas Osowski](https://blog.iota.org/welcome-andreas-c-osowski-to-iota-5f1d8ef40194), Senior Software Engineer
* [Andrew Greve](https://blog.iota.org/welcome-andrew-greve-to-the-iota-foundation-d3b897113ce4), Online Community Liason
* [Bartosz Kusmierz](https://blog.iota.org/welcome-bartosz-ku%C5%9Bmierz-to-iota-58613662f204), Research Scientist
* [Brord van Wierst](https://blog.iota.org/welcome-brord-van-wierst-to-the-iota-foundation-801cc4314db1), Full Stack Programmer
* [Casper Eicke Frederiksen](https://blog.iota.org/welcome-casper-eicke-frederiksen-to-the-iota-foundation-c7f51dc59dc), Media Producer
* [Charlie Varley](https://blog.iota.org/welcome-charlie-varley-to-the-iota-foundation-dce7b1532e98), Software Engineer
* [Chris Dukakis](https://blog.iota.org/welcome-chris-dukakis-to-iota-1db0262e422c), Software Engineer
* [Clara Shikhelman](https://blog.iota.org/welcome-clara-shikhelman-941c65bf7545), Research Mathematician
* [Darcy Camargo](https://blog.iota.org/a-mathematician-is-always-fascinated-by-a-challenge-and-with-darcy-camargo-its-no-different-72c55f996401), Research Mathematician
* [Dave De Fijter](https://blog.iota.org/welcome-dave-de-fijter-to-the-iota-foundation-d52d0ef94204), Online Community Liaison
* [Dyrell Chapman](https://blog.iota.org/welcome-dyrell-chapman-to-the-iota-foundation-7db912fa4a7a), Software Engineer
* [Edward Greve](https://blog.iota.org/welcome-edward-greve-to-the-iota-foundation-dd3a63942a91), Head of Engineering
* [Emmanuel Merali](https://blog.iota.org/welcome-emannuel-merali-to-the-iota-foundation-c0a0181a35fe), Staff Software Engineer
* [Eric Hop](https://blog.iota.org/welcome-eric-hop-to-the-iota-foundation-1e06530bb971), Senior Product Owner
* [Fahad Sheikh](https://blog.iota.org/welcome-fahad-sheikh-to-the-iota-foundation-498ba2c01ae5), Online Community Liaison
* [Gal Rogozinski](https://blog.iota.org/welcome-gal-rogozinski-to-the-iota-foundation-2be18f7cd5b0), Software Engineer
* [Giorgio E. Mandolfo](https://blog.iota.org/welcome-giorgio-e-mandolfo-to-the-iota-foundation-f6f3bb88053c), SysOps Engineer
* [Hans Moog](https://blog.iota.org/welcome-hans-moog-to-the-iota-foundation-ff8c6d64158b), working on local snapshot
* [Holger Kther](https://blog.iota.org/welcome-holger-k%C3%B6ther-to-the-iota-foundation-72c2eba8c41a), Head of Partnerships
* [Jakub Cech](https://blog.iota.org/welcome-jakub-cech-to-the-iota-foundation-2c74d47e5fb5), Software Engineer & Product Manager
* [Jan Pauseback](https://blog.iota.org/welcome-jan-pauseback-to-the-iota-foundation-505c26869513), Technical Analyst
* [Janine Hrtel](https://blog.iota.org/welcome-janine-h%C3%A4rtel-to-the-iota-foundation-219278179239), drives adoption in the mobility industry
* [Jens Munch Lund\-Nielsen](https://blog.iota.org/welcome-jens-munch-lund-nielsen-to-the-iota-foundation-c0917abc29af), Global Trade & Supply Chains Stream Lead
* [John Licciardello](https://blog.iota.org/welcome-john-licciardello-to-the-iota-foundation-fec74e93031c), Managing Director, Ecosystem Development Fund
* [Julie Maupin](https://blog.iota.org/welcome-julie-maupin-to-iota-14b9ac92478f), Head of Social Impact & Public Regulatory Affairs
* [Lewis Freiberg](https://blog.iota.org/welcome-lewis-freiberg-to-iota-6e221a4e7ac3), Head of Ecosystem
* [Mark Schmidt](https://blog.iota.org/welcome-mark-schmidt-to-the-iota-foundation-5e538595c942), Community Manager
* [Mathew Yarger](https://blog.iota.org/welcome-mathew-yarger-to-the-iota-foundation-4a1dc3c8180f), Security Development
* [Marwen Trabelsi](https://blog.iota.org/welcome-marwen-trabelsi-to-the-iota-foundation-5bf7d6840824), drives adoption in distributed gaming
* [Navin Ramachandran](https://blog.iota.org/welcome-navin-ramachandran-to-the-iota-foundation-8c3315116ce3), e\-Health Stream Lead
* [Olivia Saa](https://blog.iota.org/welcome-olivia-and-paulo-to-iota-research-796ecd683069), Research Mathematician
* [Paul Douglas](https://blog.iota.org/meet-core-developer-paul-handy-932d0ede8771), Senior Software Engineer
* [Paulo Finardi](https://blog.iota.org/welcome-olivia-and-paulo-to-iota-research-796ecd683069), Research Scientist
* [Ralf Rottmann](https://blog.iota.org/welcome-ralf-rottmann-to-the-iota-foundation-157c86011438), Member Board of Directors
* [Rihards Gravis](https://blog.iota.org/welcome-rihards-gravis-to-the-iota-foundation-566ac1ab3f70), iota.dance
* [Sabri Goldberg](https://blog.iota.org/meet-iota-graphic-designer-sabri-goldberg-f174eba5834d), Creative Director
* [Samuel Reid](https://blog.iota.org/welcome-samuel-reid-to-iota-744469bce973), Q Architect
* [Tsvi Sabo](https://blog.iota.org/welcome-tsvi-sabo-to-the-iota-foundation-caf9587f1397), Software Engineer
* [Umair Sarfraz](https://blog.iota.org/welcome-umair-sarfraz-to-the-iota-foundation-68dc5805067a), Software Engineer
* [Wilfried Pimenta de Miranda](https://blog.iota.org/welcome-wilfried-pimenta-de-miranda-to-the-iota-foundation-970a1654683e), Head of Business Development

&nbsp;

* [Anes Hodic](https://blog.iota.org/welcome-anes-hod%C5%BEi%C4%87-to-the-iota-foundation-2864f95b91b4) - Vice President Digital Transformation and Internet of Things (IoT) at Digital Transformation Office of Airbus Group
* [Alexander Renz](https://blog.iota.org/welcome-alexander-renz-to-iota-d2ee426ea967), Smart Mobility & Transportation Advisor
* [Gur Huberman](https://blog.iota.org/welcome-gur-huberman-to-the-iota-foundation-f801dd6e5410), Finance & Economics Advisor - Professor of Behavioral Finance at Columbia Business School
* [Harm van den Brink](https://blog.iota.org/welcome-harm-van-den-brink-to-the-iota-foundation-41295c7f0ccf), Energy & E\-mobility Advisor - Built the [IOTA Charging Station PoC](https://medium.com/@harmvandenbrink/how-elaadnl-built-a-poc-charge-station-running-fully-on-iota-and-iota-only-e16ed4c4d4d5)
* [Hongquan Jiang](https://blog.iota.org/welcome-huangquan-jiang-to-the-iota-foundation-3a3835e0fc06), Technology Industry Advisor - Robert Bosch Venture Capital GmbH
* [Joachim Taiber](https://blog.iota.org/welcome-joachim-taiber-to-iota-259f0a1aa34f), Smart Mobility Advisor
* [Jochen Renz](https://blog.iota.org/welcome-jochen-renz-to-the-iota-foundation-9979db6638f3), Smart Mobility & Transportation Advisor
* [Johann Jungwirth](https://twitter.com/johannjungwirth/status/955119052111872000), Supervisory Board - CDO of Volkswagen Group
* [Koen Maris](https://blog.iota.org/welcome-koen-maris-to-the-iota-foundation-e02cb75da784), Cyber Security Advisor
* [Klaus Schaaf](https://blog.iota.org/welcome-klaus-schaaf-to-the-iota-foundation-5e823b8b91e1), Advisor
* [Madjid Nakhjiri](https://blog.iota.org/welcome-madjid-nakhjiri-to-the-iota-foundation-e3fe448c367c), Public Key Infrastructure Advisor
* [Michael Nilles](https://blog.iota.org/welcome-michael-nilles-to-the-iota-foundation-403feb15bb6a), Smart Cities & Infrastructure Advisor - CDO at Schindler Group, supervisory board of Deutsche Lufthansa AG
* [Regine H. Helmer](https://blog.iota.org/welcome-regine-haschka-helmer-to-the-iota-foundation-9ff42bb56aff), Digital Transformation & Business Development Advisor
* [Dr. Richard Mark Soley](https://blog.iota.org/welcome-richard-soley-to-the-iota-foundation-eb61e015187d), Supervisory Board Member and Advisor - Chairman and CEO of [OMG](http://www.omg.org/), Executive Director of the [Cloud Standards Customer Council](http://www.cloud-council.org/) and Executive Director at [IIC](https://www.iiconsortium.org/)
* [Dr. Rolf Werner](https://blog.iota.org/welcome-rolf-werner-to-the-iota-foundation-13822ea8b281), Supervisory Board Member and Advisor - Head of Central Europe at Fujitsu

 ^(\(See also: [https://www.iota.org/the\-foundation/team](https://www.iota.org/the-foundation/team)\))

&nbsp;

___
___
# Partnerships, Affiliations & Corporate Interests

&nbsp;

**Since the list of individual entities that are interested in or affiliated with IOTA has grown too large to maintain in this thread, please head over to the excellent [IOTA Archive](http://iotaarchive.com/), created by [/u/Elchwurst](https://www.reddit.com/user/Elchwurst).**

IOTA is a founding member of the [Trusted IoT Alliance](https://www.trusted-iot.org/), member of the [Mobility Open Blockchain Initiative](https://www.dlt.mobi/) ([announcement here](https://blog.iota.org/iota-joins-the-mobility-open-blockchain-initiative-mobi-1c017a8c00f0)), member of the [Decentralized Identity Foundation](http://identity.foundation/) ([announcement here](https://twitter.com/iotatoken/status/866727260035985410)) as well as the [Blockchain Bundesverband e.V.](https://www.bundesblock.de/) ([announcement here](https://twitter.com/iotatoken/status/866727260035985410)) and the [Startup Autobahn](http://www.startup-autobahn.com/en/news/130-joint-projects-between-startups-and-corporations-initiated-but-still-hungry-startup-autobahn-launches-program-4/) program. 

[Dr. Julie Maupin](https://blog.iota.org/welcome-julie-maupin-to-iota-14b9ac92478f) and [Oliver Bussmann](https://blog.iota.org/welcome-oliver-t-bussmann-to-iota-4fa2614eaca7) from the IOTA Foundation as well as Jamie Burke (Outlier Ventures, IOTA investor) have been named members at the [European Blockchain Observatory and Forum](https://www.eublockchainforum.eu/news/eu-blockchain-observatory-and-forum-names-members-core-working-groups).

&nbsp;

___
___
# Archive of Noteworthy Stuff

&nbsp;

* *May 03, 2018* - **Q**ubic [revealed](https://qubic.iota.org/)!
* [IOTA \- 100 Billion Reasons Why with Terry Shane](https://www.youtube.com/watch?v=0KDRc23Vj7w) - Excellent presentation, watch this!
* [Ivan on Tech \- Interview with David Snsteb](https://www.youtube.com/watch?v=GwhJQ67zxbg)
* [Interview with David Snsteb](https://www.youtube.com/watch?v=T2FJ9hH66b8)
* [MAKERS Rooftop talk with Dominik Schiener](https://www.youtube.com/watch?v=EXjCqT-oK9M)
* [Tokyo Meetup with Dominik Schiener](https://www.youtube.com/watch?v=OdxGWXjwaG4)
* [CEO of Fujitsu talks about IOTA](https://www.youtube.com/watch?v=GLzLTWwO26s)
* [Blockchain Podcast #29 with Dominik Schiener](https://www.youtube.com/watch?v=wiMJa9lrLvw)
* [Ether Review \#69](https://thebitcoinpodcast.com/ter69/) - IOTA & the Post-Blockchain Era with David Snsteb
* *January 7, 2018* - [AMA with the IOTA Foundation](https://www.reddit.com/r/Iota/comments/7orp03/iota_foundation_ask_us_anything_january_7th/)
* *October 21, 2015* - [IOTA Announcement](https://bitcointalk.org/index.php?topic=1216479.0) in the Bitcointalk forum.
* *April 13, 2015* - [JINN Announcement](https://nxtforum.org/news-and-announcements/(ann)-jinn/1080/?PHPSESSID=nb18sq4loq6s72omdr7ib39ta0) at Bitcointalk.
* *September 25, 2012* - [Qubic Announcement](https://bitcointalk.org/index.php?topic=112676.0) at Bitcointalk. This ultimatively led to the birth of IOTA years later.
