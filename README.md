## /r/IOTA All-in-One Thread

This is the GitLab repo for the stickied [/r/iota](https://www.reddit.com/r/Iota/) All-in-One Thread. It can be found [here](https://www.reddit.com/r/Iota/comments/8kfrxk/iota_allinone_thread/).


The thread was started May 19, 2018. This repo contains the whole history of the thread from two weeks after the starting date.


Both thread and repo are maintained by [/u/Aconitin](https://www.reddit.com/user/Aconitin).